package com.achmad.baseandroid.service.data.mapper

import com.achmad.baseandroid.service.data.entity.ArticleEntity
import com.achmad.baseandroid.service.data.entity.NewsResponseEntity
import com.achmad.baseandroid.service.data.entity.SourceEntity
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.service.data.model.NewsResponse
import com.achmad.baseandroid.service.data.model.Source
import org.junit.Assert
import org.junit.Test

internal class MapperKtTest {

    @Test
    fun `test mapToNewsResponse()`() {
        val entity = NewsResponseEntity(
            status = "ok",
            totalResults = 10,
            articles = listOf(),
            sources = listOf()
        )

        val actual = entity.mapToNewsResponse()

        val expected = NewsResponse(
            status = "ok",
            totalResults = 10,
            articles = listOf(),
            sources = listOf()
        )

        Assert.assertEquals(expected, actual)
    }

    @Test
    fun `test mapToSource()`() {
        val entity = SourceEntity(
            id = "123",
            name = "name",
            description = "desc",
            url = "url",
            category = null,
            language = null,
            country = null,
        )

        val actual = entity.mapToSource()

        val expected = Source(
            id = "123",
            name = "name",
            description = "desc",
            url = "url",
            category = "",
            language = "",
            country = "",
        )

        Assert.assertEquals(expected, actual)
    }

    @Test
    fun `test mapToArticle()`() {
        val entity = ArticleEntity(
            title = "title",
            author = "author",
            description = null,
            url = null,
            publishedAt = null
        )

        val actual = entity.mapToArticle()

        val expected = Article(
            title = "title",
            author = "author",
            description = "",
            url = "",
            publishedAt = ""
        )

        Assert.assertEquals(expected, actual)
    }
}
