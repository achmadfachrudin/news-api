package com.achmad.baseandroid.service.data

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.entity.ArticleEntity
import com.achmad.baseandroid.service.data.entity.NewsResponseEntity
import com.achmad.baseandroid.service.data.entity.SourceEntity
import com.achmad.baseandroid.service.data.model.CategoryEnum
import com.skydoves.sandwich.ApiResponse
import io.mockk.coEvery
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import retrofit2.Response

internal class FetchArticlesBySourceTest : AppRepositoryTest() {

    private val page = ServiceConstant.INITIAL_PAGE
    private val query = ""
    private val sourceId = "123"

    @Test
    fun `fetchArticlesBySource success should return data`() {
        val result = ArticleEntity(
            title = "title",
            author = "author",
            description = null,
            url = null,
            publishedAt = null
        )

        coEvery {
            remote.fetchArticlesBySource(any(), any(), any())
        } returns ApiResponse.Success(
            Response.success(
                NewsResponseEntity(
                    status = "ok",
                    articles = listOf(result)
                )
            )
        )

        val actual = runBlocking {
            repository.fetchArticlesBySource(
                page = page,
                query = query,
                sourceId = sourceId,
            ).last()
        }

        Assert.assertEquals(ApiResult.Success(result).status, actual.status)
        Assert.assertEquals(1, actual.data?.size)
    }

    @Test
    fun `fetchArticlesBySource error should return error`() {
        val result = Response.error<NewsResponseEntity>(
            400,
            "".toResponseBody()
        )

        coEvery {
            remote.fetchArticlesBySource(any(), any(), any())
        } returns ApiResponse.Failure.Error(result)

        val actual = runBlocking {
            repository.fetchArticlesBySource(
                page = page,
                query = query,
                sourceId = sourceId,
            ).last()
        }

        Assert.assertEquals(ApiResult.Error("").status, actual.status)
    }
}
