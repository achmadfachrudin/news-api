package com.achmad.baseandroid.service.data

import io.mockk.clearAllMocks
import io.mockk.mockk
import org.junit.Before

open class AppRepositoryTest {

    protected val remote: AppRemote = mockk()

    protected val repository = AppRepository(
        remote
    )

    @Before
    fun setup() {
        clearAllMocks()
    }
}
