package com.achmad.baseandroid.service.data

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.entity.NewsResponseEntity
import com.achmad.baseandroid.service.data.entity.SourceEntity
import com.achmad.baseandroid.service.data.model.CategoryEnum
import com.skydoves.sandwich.ApiResponse
import io.mockk.coEvery
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import retrofit2.Response

internal class FetchSourcesByCategoryTest : AppRepositoryTest() {

    private val categoryQuery = CategoryEnum.BUSINESS.query

    @Test
    fun `fetchSourceByCategory success should return data`() {
        val result = SourceEntity(
            id = "123",
            name = "name",
            description = "desc",
            url = "url",
            category = "",
            language = "",
            country = "",
        )

        coEvery {
            remote.fetchSourcesByCategory(any())
        } returns ApiResponse.Success(
            Response.success(
                NewsResponseEntity(
                    status = "ok",
                    sources = listOf(result)
                )
            )
        )

        val actual = runBlocking { repository.fetchSourcesByCategory(categoryQuery).last() }

        Assert.assertEquals(ApiResult.Success(result).status, actual.status)
        Assert.assertEquals(1, actual.data?.size)
    }

    @Test
    fun `fetchSourceByCategory error should return error`() {
        val result = Response.error<NewsResponseEntity>(
            400,
            "".toResponseBody()
        )

        coEvery {
            remote.fetchSourcesByCategory(any())
        } returns ApiResponse.Failure.Error(result)

        val actual = runBlocking { repository.fetchSourcesByCategory(categoryQuery).last() }

        Assert.assertEquals(ApiResult.Error("").status, actual.status)
    }
}
