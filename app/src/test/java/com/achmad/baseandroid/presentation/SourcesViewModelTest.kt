package com.achmad.baseandroid.presentation

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.AppRepository
import com.achmad.baseandroid.service.data.model.CategoryEnum
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.service.domain.GetSourcesByCategoriesUseCase
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class SourcesViewModelTest {

    private val repository = mockk<AppRepository>()
    private val usecase = mockk<GetSourcesByCategoriesUseCase>()

    private val viewModel = SourcesViewModel(
        usecase,
    )

    private val model = Source(
        id = "123",
        name = "name",
        description = "desc",
        url = "url",
        category = "",
        language = "",
        country = "",
    )

    @Before
    fun setup() {
        clearAllMocks()
    }

    @Test
    fun `fetchFavoriteList success should return data`() = runTest {
        val postList = listOf(model)

        val result = ApiResult.Success(postList)
        coEvery {
            repository.fetchSourcesByCategory(any())
        } returns flowOf(result)

        val collectJob = launch(UnconfinedTestDispatcher()) {
            repository.fetchSourcesByCategory(CategoryEnum.BUSINESS.query).collectLatest { viewModel.uiMutableState.value = it }
        }

        Assert.assertEquals(result.status, viewModel.uiState.value.status)

        collectJob.cancel()
    }
}
