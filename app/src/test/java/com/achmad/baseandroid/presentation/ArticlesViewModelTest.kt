package com.achmad.baseandroid.presentation

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.AppRepository
import com.achmad.baseandroid.service.data.ServiceConstant
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.service.data.model.CategoryEnum
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.service.domain.GetArticlesBySourcesUseCase
import com.achmad.baseandroid.service.domain.GetSourcesByCategoriesUseCase
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class ArticlesViewModelTest {

    private val repository = mockk<AppRepository>()
    private val usecase = mockk<GetArticlesBySourcesUseCase>()

    private val viewModel = ArticlesViewModel(
        usecase,
    )

    private val page = ServiceConstant.INITIAL_PAGE
    private val query = ""
    private val sourceId = "123"

    private val model = Article(
        title = "title",
        author = "author",
        description = "",
        url = "",
        publishedAt = ""
    )

    @Before
    fun setup() {
        clearAllMocks()
    }

    @Test
    fun `fetchArticlesBySource success should return data`() = runTest {
        val result = ApiResult.Success(listOf(model))
        coEvery {
            repository.fetchArticlesBySource(any(), any(), any())
        } returns flowOf(result)

        val collectJob = launch(UnconfinedTestDispatcher()) {
            repository.fetchArticlesBySource(
                page = page,
                query = query,
                sourceId = sourceId,
            ).collectLatest {
                it.data?.let { articles ->
                    viewModel.uiMutableState.value = ArticlesViewModel.State(
                        displayState = ArticlesViewModel.State.DisplayState.Content(
                            articles.map { ArticleItemResult.ContentItemVM(it) }
                        )
                    )
                }
            }
        }

        Assert.assertEquals(
            ArticlesViewModel.State.DisplayState.Content(
                listOf(ArticleItemResult.ContentItemVM(model))
            ),
            viewModel.uiState.value.displayState
        )

        collectJob.cancel()
    }
}
