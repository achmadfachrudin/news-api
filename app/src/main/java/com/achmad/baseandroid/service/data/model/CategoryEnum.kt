package com.achmad.baseandroid.service.data.model

import androidx.annotation.Keep

@Keep
enum class CategoryEnum(val query: String, val title: String) {
    BUSINESS("business", "Business"),
    ENTERTAINMENT("entertainment", "Entertainment"),
    GENERAL("general", "General"),
    HEALTH("health", "Health"),
    SCIENCE("science", "Science"),
    SPORTS("sports", "Sports"),
    TECHNOLOGY("technology", "Technology")
}
