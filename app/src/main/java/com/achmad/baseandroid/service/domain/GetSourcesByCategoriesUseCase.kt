package com.achmad.baseandroid.service.domain

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.AppRepository
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.Source
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetSourcesByCategoriesUseCase @Inject constructor(
    private val repository: AppRepository
) {
    operator fun invoke(categoryQuery: String): Flow<ApiResult<List<Source>>> {
        return repository.fetchSourcesByCategory(categoryQuery)
    }
}