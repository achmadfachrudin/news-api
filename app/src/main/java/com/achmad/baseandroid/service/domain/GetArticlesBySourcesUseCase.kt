package com.achmad.baseandroid.service.domain

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.AppRepository
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.Source
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetArticlesBySourcesUseCase @Inject constructor(
    private val repository: AppRepository
) {
    operator fun invoke(
        query: String,
        sourceId: String,
        page: Int,
    ): Flow<ApiResult<List<Article>>> {
        return repository.fetchArticlesBySource(
            query = query,
            sourceId = sourceId,
            page = page,
        )
    }
}