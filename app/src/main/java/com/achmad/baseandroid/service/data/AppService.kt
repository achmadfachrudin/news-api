package com.achmad.baseandroid.service.data

import com.achmad.baseandroid.service.data.ServiceConstant.PAGE_SIZE
import com.achmad.baseandroid.service.data.entity.NewsResponseEntity
import com.skydoves.sandwich.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {

    @GET("top-headlines/sources")
    suspend fun fetchSourcesByCategory(
        @Query("category") category: String,
    ): ApiResponse<NewsResponseEntity>

    @GET("top-headlines")
    suspend fun fetchArticlesBySource(
        @Query("q") query: String,
        @Query("sources") sources: String,
        @Query("page") page: Int,
        @Query("pageSize") pagSize: Int = PAGE_SIZE,
    ): ApiResponse<NewsResponseEntity>
}
