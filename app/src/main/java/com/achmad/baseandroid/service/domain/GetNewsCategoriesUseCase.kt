package com.achmad.baseandroid.service.domain

import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.CategoryEnum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetNewsCategoriesUseCase @Inject constructor() {
    operator fun invoke(): Flow<List<Category>> {
        return flow {
            val categoryEnumList = CategoryEnum.values().toList()
            val categoryList = categoryEnumList.map { Category(it.query, it.title) }

            emit(categoryList)
        }.flowOn(Dispatchers.IO)
    }
}