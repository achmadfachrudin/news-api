package com.achmad.baseandroid.service.data

import com.achmad.baseandroid.service.data.entity.NewsResponseEntity
import com.skydoves.sandwich.ApiResponse
import javax.inject.Inject

class AppRemote @Inject constructor(
    private val service: AppService
) {
    suspend fun fetchSourcesByCategory(
        category: String,
    ): ApiResponse<NewsResponseEntity> = service.fetchSourcesByCategory(category)

    suspend fun fetchArticlesBySource(
        query: String,
        sourceId: String,
        page: Int,
    ): ApiResponse<NewsResponseEntity> = service.fetchArticlesBySource(query, sourceId, page)
}
