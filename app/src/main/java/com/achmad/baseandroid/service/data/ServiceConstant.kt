package com.achmad.baseandroid.service.data

object ServiceConstant {
    const val INITIAL_PAGE = 1
    const val PAGE_SIZE = 10
}
