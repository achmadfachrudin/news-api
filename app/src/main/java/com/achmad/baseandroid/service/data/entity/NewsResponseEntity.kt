package com.achmad.baseandroid.service.data.entity

import androidx.annotation.Keep
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class NewsResponseEntity(
    @field:Json(name = "status") val status: String? = null,
    @field:Json(name = "totalResults") val totalResults: Int? = null,
    @field:Json(name = "articles") val articles: List<ArticleEntity>? = null,
    @field:Json(name = "sources") val sources: List<SourceEntity>? = null,
)
