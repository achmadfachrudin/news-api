package com.achmad.baseandroid.service.data

import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.mapper.mapToArticle
import com.achmad.baseandroid.service.data.mapper.mapToSource
import com.achmad.baseandroid.service.data.model.Article
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val remote: AppRemote
) {

    fun fetchSourcesByCategory(
        categoryQuery: String,
    ) = flow {
        emit(ApiResult.Loading)

        val response = remote.fetchSourcesByCategory(categoryQuery)

        response.suspendOnSuccess {
            if (data.status == "ok") {
                val result = data.sources?.map { it.mapToSource() } ?: emptyList()
                emit(ApiResult.Success(result))
            } else {
                emit(ApiResult.Error("empty result"))
            }
        }.suspendOnError {
            val code = this.raw.code
            val status = this.statusCode.name
            emit(ApiResult.Error("$code $status"))
        }.suspendOnException {
            emit(ApiResult.Error(this.exception.message.orEmpty()))
        }
    }.flowOn(Dispatchers.IO)

    fun fetchArticlesBySource(
        query: String,
        sourceId: String,
        page: Int,
    ) = flow {
        emit(ApiResult.Loading)

        val response = remote.fetchArticlesBySource(query, sourceId, page)

        response.suspendOnSuccess {
            if (data.status == "ok") {
                val result = data.articles?.map { it.mapToArticle() } ?: emptyList<Article>()
                    .distinctBy { it.url }
                emit(ApiResult.Success(result))
            } else {
                emit(ApiResult.Error("empty result"))
            }
        }.suspendOnError {
            val code = this.raw.code
            val status = this.statusCode.name
            emit(ApiResult.Error("$code $status"))
        }.suspendOnException {
            emit(ApiResult.Error(this.exception.message.orEmpty()))
        }
    }.flowOn(Dispatchers.IO)
}
