package com.achmad.baseandroid.service.di

import com.achmad.baseandroid.service.data.AppRepository
import com.achmad.baseandroid.service.domain.GetArticlesBySourcesUseCase
import com.achmad.baseandroid.service.domain.GetNewsCategoriesUseCase
import com.achmad.baseandroid.service.domain.GetSourcesByCategoriesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DomainModule {

    @Provides
    fun provideGetNewsCategoriesUseCase(repository: AppRepository) = GetNewsCategoriesUseCase()

    @Provides
    fun provideGetSourcesByCategoriesUseCase(repository: AppRepository) = GetSourcesByCategoriesUseCase(repository)

    @Provides
    fun provideGetArticlesBySourcesUseCase(repository: AppRepository) = GetArticlesBySourcesUseCase(repository)
}
