package com.achmad.baseandroid.service.data.model

import androidx.annotation.Keep

@Keep
data class NewsResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>,
    val sources: List<Source>,
)
