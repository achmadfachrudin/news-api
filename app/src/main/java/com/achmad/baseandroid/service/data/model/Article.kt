package com.achmad.baseandroid.service.data.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Article(
    val title: String,
    val author: String,
    val description: String,
    val url: String,
    val publishedAt: String
) : Parcelable
