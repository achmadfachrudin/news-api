package com.achmad.baseandroid.service.data.model

import androidx.annotation.Keep

@Keep
data class Category(
    val query: String,
    val title: String
)
