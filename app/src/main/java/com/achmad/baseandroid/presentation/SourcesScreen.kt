package com.achmad.baseandroid.presentation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.achmad.baseandroid.R
import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseColor
import com.achmad.baseandroid.theme.component.BaseOutlinedTextField
import com.achmad.baseandroid.theme.component.BaseSpacerVertical
import com.achmad.baseandroid.theme.component.BaseText
import com.achmad.baseandroid.theme.component.BaseToolbar
import com.achmad.baseandroid.theme.component.EmptyState
import com.achmad.baseandroid.theme.component.ErrorState
import com.achmad.baseandroid.theme.component.LoadingState

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun SourcesScreen(
    viewModel: SourcesViewModel,
    onLeftButtonClick: () -> Unit,
    onQueryChanged: (newQuery: String) -> Unit,
    onItemClick: (model: Source) -> Unit,
) {
    val uiState = viewModel.uiState.collectAsState().value
    val queryState = viewModel.queryState.collectAsState().value

    Scaffold(
        topBar = {
            BaseToolbar(
                title = stringResource(id = R.string.title_feature_sources),
                onLeftButtonClick = onLeftButtonClick
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
                    .padding(16.dp),
            ) {
                when (uiState) {
                    ApiResult.Loading -> {
                        LoadingState()
                    }

                    is ApiResult.Error -> {
                        ErrorState(message = uiState._error)
                    }

                    is ApiResult.Success -> {
                        LazyColumn(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.spacedBy(16.dp)
                        ) {
                            stickyHeader {
                                BaseOutlinedTextField(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .background(BaseColor.White)
                                        .padding(16.dp),
                                    placeholder = {
                                        BaseText(text = stringResource(id = R.string.title_feature_sources_search))
                                    },
                                    value = queryState,
                                    onValueChange = { newQuery ->
                                        onQueryChanged(newQuery)
                                    }
                                )

                                BaseSpacerVertical()
                            }

                            val items = uiState._data.filter { it.name.lowercase().contains(queryState) }

                            items(items) { model ->
                                SourceCard(model) { onItemClick(model) }
                            }
                        }
                    }
                }
            }
        }
    )
}

@Composable
private fun SourceCard(
    model: Source,
    onItemClick: () -> Unit,
) {
    Card {
        Row(
            modifier = Modifier
                .clickable(onClick = onItemClick)
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            BaseText(
                text = model.name
            )
        }
    }
}

