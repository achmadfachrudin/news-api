package com.achmad.baseandroid.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.ServiceConstant.INITIAL_PAGE
import com.achmad.baseandroid.service.data.ServiceConstant.PAGE_SIZE
import com.achmad.baseandroid.service.domain.GetArticlesBySourcesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticlesViewModel @Inject constructor(
    private val getArticlesBySourcesUseCase: GetArticlesBySourcesUseCase
) : ViewModel() {

    data class State(
        val sourceId: String = "",
        val page: Int = INITIAL_PAGE,
        val displayState: DisplayState = DisplayState.Loading,
        val searchQuery: String = "",
    ) {
        sealed class DisplayState {
            data class Error(val message: String) : DisplayState()
            object Loading : DisplayState()
            data class Content(val items: List<ArticleItemResult>) : DisplayState()
        }
    }

    val uiMutableState = MutableStateFlow(State())
    val uiState: StateFlow<State> = uiMutableState

    val message = MutableLiveData<String>()

    var searchJob: Job ?= null

    fun onViewed(sourceId: String) {
        viewModelScope.launch { uiMutableState.value = State(sourceId = sourceId) }
        getArticles()
    }

    fun onQueryChanged(newQuery: String) {
        uiMutableState.value = uiMutableState.value.copy(searchQuery = newQuery.trim().lowercase())

        searchJob = viewModelScope.launch {
            delay(300)
            getArticles()
        }
    }

    fun getArticles() {
        viewModelScope.launch {
            getArticlesBySourcesUseCase(
                query = uiState.value.searchQuery,
                sourceId = uiState.value.sourceId,
                page = uiState.value.page,
            ).collectLatest {
                val displayState = when (it) {
                    is ApiResult.Error -> State.DisplayState.Error(it._error)
                    ApiResult.Loading -> State.DisplayState.Loading
                    is ApiResult.Success -> {
                        val newContents = mutableListOf<ArticleItemResult>()
                        newContents.addAll(it._data.map { article -> ArticleItemResult.ContentItemVM(article) })

                        if (it._data.isNotEmpty() && it._data.size == PAGE_SIZE) {
                            newContents.add(ArticleItemResult.LoadMoreItemVM)
                        }

                        State.DisplayState.Content(newContents)
                    }
                }

                uiMutableState.value = State(
                    sourceId = uiState.value.sourceId,
                    page = INITIAL_PAGE,
                    displayState = displayState,
                    searchQuery = uiState.value.searchQuery
                )
            }
        }
    }

    fun loadMore() {
        viewModelScope.launch {
            val page = uiState.value.page + 1

            getArticlesBySourcesUseCase(
                query = uiState.value.searchQuery,
                sourceId = uiState.value.sourceId,
                page = page,
            ).collectLatest {
                when (it) {
                    is ApiResult.Error -> message.value = it._error
                    ApiResult.Loading -> {
                    }
                    is ApiResult.Success -> {
                        val newContents = mutableListOf<ArticleItemResult>()
                        val oldItems = (uiState.value.displayState as State.DisplayState.Content)
                            .items
                            .filterIsInstance<ArticleItemResult.ContentItemVM>()

                        newContents.addAll(oldItems)
                        newContents.addAll(it._data.map { article -> ArticleItemResult.ContentItemVM(article) })


                        if (it._data.isNotEmpty() && it._data.size == PAGE_SIZE) {
                            newContents.add(ArticleItemResult.LoadMoreItemVM)
                        }

                        val displayState = State.DisplayState.Content(newContents)

                        uiMutableState.value = State(
                            sourceId = uiState.value.sourceId,
                            page = page,
                            displayState = displayState,
                            searchQuery = uiState.value.searchQuery
                        )
                    }
                }
            }
        }
    }
}
