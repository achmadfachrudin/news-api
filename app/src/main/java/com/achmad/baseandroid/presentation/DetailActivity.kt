package com.achmad.baseandroid.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.achmad.baseandroid.core.extension.showToast
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.theme.BaseComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_MODEL = "EXTRA_MODEL"

        fun createIntent(
            context: Context,
            model: Article,
        ): Intent {
            return Intent(context, DetailActivity::class.java).apply {
                putExtra(EXTRA_MODEL, model)
            }
        }
    }

    private val model: Article? by lazy { intent.getParcelableExtra(EXTRA_MODEL) }

    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            BaseComposeTheme {
                model?.let { movie ->
                    DetailScreen(
                        viewModel = viewModel,
                        model = movie,
                        onLeftButtonClick = { onBackPressed() },
                        onRightButtonClick = {
                        }
                    )
                }
            }
        }

        initObservers()
    }

    private fun initObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.message.observe(this@DetailActivity) {
                    if (it.isNotEmpty()) showToast(it)
                }
            }
        }
    }
}
