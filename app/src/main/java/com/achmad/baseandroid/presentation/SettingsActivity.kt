package com.achmad.baseandroid.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import com.achmad.baseandroid.cache.PreferenceManager
import com.achmad.baseandroid.theme.BaseComposeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsActivity : AppCompatActivity() {

    companion object {
        fun createIntent(
            context: Context,
        ): Intent {
            return Intent(context, SettingsActivity::class.java).apply {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            BaseComposeTheme {
                SettingsScreen(
                    onLeftButtonClick = { onBackPressed() },
                    onLightClick = { applyLight() },
                    onDarkClick = { applyDark() },
                )
            }
        }
    }

    private fun applyLight() {
        PreferenceManager.isSystemInDarkTheme = false
        startActivity(MainActivity.createIntent(this))
    }

    private fun applyDark() {
        PreferenceManager.isSystemInDarkTheme = true
        startActivity(MainActivity.createIntent(this))
    }
}
