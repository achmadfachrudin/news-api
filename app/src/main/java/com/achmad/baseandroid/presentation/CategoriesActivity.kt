package com.achmad.baseandroid.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseComposeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoriesActivity : AppCompatActivity() {

    companion object {
        fun createIntent(
            context: Context,
        ): Intent {
            return Intent(context, CategoriesActivity::class.java).apply {
            }
        }
    }

    private val viewModel: CategoriesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            BaseComposeTheme {
                CategoriesScreen(
                    viewModel = viewModel,
                    onLeftButtonClick = { onBackPressed() },
                    onItemClick = { goToSource(it.query) }
                )
            }
        }

        viewModel.getCategories()
    }

    private fun goToSource(categoryQuery: String) {
        startActivity(
            SourcesActivity.createIntent(
                this,
                categoryQuery
            )
        )
    }
}
