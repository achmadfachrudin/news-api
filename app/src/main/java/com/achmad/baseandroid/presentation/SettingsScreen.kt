package com.achmad.baseandroid.presentation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.achmad.baseandroid.R
import com.achmad.baseandroid.theme.component.BaseButton
import com.achmad.baseandroid.theme.component.BaseSpacerVertical
import com.achmad.baseandroid.theme.component.BaseToolbar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    onLeftButtonClick: () -> Unit,
    onLightClick: () -> Unit,
    onDarkClick: () -> Unit,
) {
    Scaffold(
        topBar = {
            BaseToolbar(
                title = stringResource(id = R.string.title_feature_settings),
                onLeftButtonClick = onLeftButtonClick,
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
                    .padding(16.dp),
            ) {
                BaseButton(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.title_feature_theme_light),
                    onClick = { onLightClick() }
                )

                BaseSpacerVertical()

                BaseButton(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.title_feature_theme_dark),
                    onClick = { onDarkClick() }
                )
            }
        }
    )
}