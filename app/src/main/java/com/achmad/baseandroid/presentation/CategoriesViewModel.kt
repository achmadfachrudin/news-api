package com.achmad.baseandroid.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.domain.GetNewsCategoriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoriesViewModel @Inject constructor(
    private val getNewsCategoriesUseCase: GetNewsCategoriesUseCase
) : ViewModel() {

    val uiMutableState = MutableStateFlow<List<Category>>(listOf())
    val uiState: StateFlow<List<Category>> = uiMutableState

    fun getCategories() {
        viewModelScope.launch {
            getNewsCategoriesUseCase().collectLatest {
                uiMutableState.value = it
            }
        }
    }
}
