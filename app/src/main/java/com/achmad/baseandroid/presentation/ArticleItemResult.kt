package com.achmad.baseandroid.presentation

import com.achmad.baseandroid.service.data.model.Article

sealed class ArticleItemResult {
    data class ContentItemVM(
        val article: Article,
    ) : ArticleItemResult()

    object LoadMoreItemVM : ArticleItemResult() {
        val id: String
            get() = "load-more"
    }
}
