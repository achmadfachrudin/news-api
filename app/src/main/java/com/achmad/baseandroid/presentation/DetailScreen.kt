package com.achmad.baseandroid.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.achmad.baseandroid.R
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.theme.BaseTextStyle
import com.achmad.baseandroid.theme.component.BaseButton
import com.achmad.baseandroid.theme.component.BaseSpacerVertical
import com.achmad.baseandroid.theme.component.BaseText
import com.achmad.baseandroid.theme.component.BaseToolbar
import com.achmad.baseandroid.theme.component.ComposeWebView

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailScreen(
    viewModel: DetailViewModel,
    model: Article,
    onLeftButtonClick: () -> Unit,
    onRightButtonClick: () -> Unit,
) {
    val uiState = viewModel.uiState.collectAsState().value

    Scaffold(
        topBar = {
            BaseToolbar(
                title = model.title,
                onLeftButtonClick = onLeftButtonClick,
                showRightButton = false,
                onRightButtonClick = onRightButtonClick,
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
                    .padding(16.dp),
            ) {

                ComposeWebView(url = model.url)
            }
        }
    )
}

