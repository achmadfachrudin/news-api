package com.achmad.baseandroid.presentation

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.achmad.baseandroid.R
import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseColor
import com.achmad.baseandroid.theme.component.BaseText
import com.achmad.baseandroid.theme.component.BaseToolbar
import com.achmad.baseandroid.theme.component.EmptyState
import com.achmad.baseandroid.theme.component.ErrorState
import com.achmad.baseandroid.theme.component.LoadingState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoriesScreen(
    viewModel: CategoriesViewModel,
    onLeftButtonClick: () -> Unit,
    onItemClick: (model: Category) -> Unit
) {
    val uiState = viewModel.uiState.collectAsState().value

    Scaffold(
        topBar = {
            BaseToolbar(
                title = stringResource(id = R.string.title_feature_categories),
                onLeftButtonClick = onLeftButtonClick
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
                    .padding(16.dp),
            ) {
                LazyColumn(verticalArrangement = Arrangement.spacedBy(16.dp)) {
                    items(uiState) { model ->
                        CategoryCard(model) { onItemClick(model) }
                    }
                }
            }
        }
    )
}

@Composable
private fun CategoryCard(
    model: Category,
    onItemClick: () -> Unit,
) {
    Card {
        Row(
            modifier = Modifier
                .clickable(onClick = onItemClick)
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            BaseText(
                text = model.title
            )
        }
    }
}

