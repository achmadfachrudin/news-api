package com.achmad.baseandroid.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.achmad.baseandroid.core.extension.showToast
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ArticlesActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_SOURCE_ID = "EXTRA_SOURCE_ID"

        fun createIntent(
            context: Context,
            sourceId: String,
        ): Intent {
            return Intent(context, ArticlesActivity::class.java).apply {
                putExtra(EXTRA_SOURCE_ID, sourceId)
            }
        }
    }

    private val sourceId: String by lazy { intent.getStringExtra(EXTRA_SOURCE_ID).orEmpty() }

    private val viewModel: ArticlesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            BaseComposeTheme {
                ArticlesScreen(
                    viewModel = viewModel,
                    onLeftButtonClick = { onBackPressed() },
                    onQueryChanged = { viewModel.onQueryChanged(it)},
                    onItemClick = { goToDetail(it) },
                    onLoadMore = { viewModel.loadMore() }
                )
            }
        }

        initObservers()

        viewModel.onViewed(sourceId)
    }

    private fun initObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.message.observe(this@ArticlesActivity) {
                    if (it.isNotEmpty()) showToast(it)
                }
            }
        }
    }

    private fun goToDetail(article: Article) {
        startActivity(
            DetailActivity.createIntent(
                this,
                article
            )
        )
    }
}
