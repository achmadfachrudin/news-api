package com.achmad.baseandroid.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseComposeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SourcesActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_CATEGORY_QUERY = "EXTRA_CATEGORY_QUERY"

        fun createIntent(
            context: Context,
            categoryQuery: String,
        ): Intent {
            return Intent(context, SourcesActivity::class.java).apply {
                putExtra(EXTRA_CATEGORY_QUERY, categoryQuery)
            }
        }
    }

    private val categoryQuery: String by lazy { intent.getStringExtra(EXTRA_CATEGORY_QUERY).orEmpty() }

    private val viewModel: SourcesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            BaseComposeTheme {
                SourcesScreen(
                    viewModel = viewModel,
                    onLeftButtonClick = { onBackPressed() },
                    onQueryChanged = { viewModel.onQueryChanged(it)},
                    onItemClick = { goToArticles(it.id) }
                )
            }
        }

        viewModel.getSources(categoryQuery)
    }

    private fun goToArticles(sourceId: String) {
        startActivity(
            ArticlesActivity.createIntent(
                this,
                sourceId
            )
        )
    }
}
