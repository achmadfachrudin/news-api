package com.achmad.baseandroid.presentation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.achmad.baseandroid.R
import com.achmad.baseandroid.service.data.model.Article
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.theme.BaseColor
import com.achmad.baseandroid.theme.component.BaseOutlinedTextField
import com.achmad.baseandroid.theme.component.BaseSpacerVertical
import com.achmad.baseandroid.theme.component.BaseText
import com.achmad.baseandroid.theme.component.BaseToolbar
import com.achmad.baseandroid.theme.component.ErrorState
import com.achmad.baseandroid.theme.component.LoadingState

private const val ARTICLE_ITEM = "article_"
private const val LOAD_MORE = "load_more"

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun ArticlesScreen(
    viewModel: ArticlesViewModel,
    onLeftButtonClick: () -> Unit,
    onQueryChanged: (newQuery: String) -> Unit,
    onItemClick: (model: Article) -> Unit,
    onLoadMore: () -> Unit
) {
    val uiState = viewModel.uiState.collectAsState().value

    Scaffold(
        topBar = {
            BaseToolbar(
                title = stringResource(id = R.string.title_feature_articles),
                onLeftButtonClick = onLeftButtonClick
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
                    .padding(16.dp),
            ) {
                BaseOutlinedTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(BaseColor.White)
                        .padding(16.dp),
                    placeholder = {
                        BaseText(text = stringResource(id = R.string.title_feature_articles_search))
                    },
                    value = uiState.searchQuery,
                    onValueChange = { newQuery ->
                        onQueryChanged(newQuery)
                    }
                )

                BaseSpacerVertical()

                when (val display = uiState.displayState) {
                    ArticlesViewModel.State.DisplayState.Loading -> {
                        LoadingState()
                    }

                    is ArticlesViewModel.State.DisplayState.Error -> {
                        ErrorState(message = display.message)
                    }

                    is ArticlesViewModel.State.DisplayState.Content -> {
                        val listState = rememberLazyListState()
                        val isLoadMoreItemVisible by remember(uiState.displayState) {
                            derivedStateOf {
                                val loadMoreItem =
                                    listState.layoutInfo.visibleItemsInfo.firstOrNull {
                                        it.key == LOAD_MORE
                                    }

                                loadMoreItem != null
                            }
                        }

                        LazyColumn(
                            modifier = Modifier.fillMaxSize(),
                            state = listState,
                            verticalArrangement = Arrangement.spacedBy(16.dp)
                        ) {
                            when (val display = uiState.displayState) {
                                is ArticlesViewModel.State.DisplayState.Content -> {
                                    itemsIndexed(items = display.items, key = { index, item ->
                                        when (item) {
                                            is ArticleItemResult.ContentItemVM -> ARTICLE_ITEM + "${index}_${item.article.url}"
                                            is ArticleItemResult.LoadMoreItemVM -> LOAD_MORE
                                        }
                                    }) { _, item ->
                                        when (item) {
                                            is ArticleItemResult.ContentItemVM -> {
                                                ArticleCard(
                                                    model = item.article,
                                                    onItemClick = { onItemClick(item.article) }
                                                )
                                            }

                                            ArticleItemResult.LoadMoreItemVM -> LoadingState()
                                        }
                                    }
                                }

                                is ArticlesViewModel.State.DisplayState.Error -> item {
                                    ErrorState(message = display.message)
                                }

                                ArticlesViewModel.State.DisplayState.Loading -> item {
                                    LoadingState()
                                }
                            }
                        }

                        LaunchedEffect(key1 = isLoadMoreItemVisible) {
                            if (isLoadMoreItemVisible) {
                                onLoadMore()
                            }
                        }
                    }
                }
            }
        }
    )
}

@Composable
private fun ArticleCard(
    model: Article,
    onItemClick: () -> Unit,
) {
    Card {
        Row(
            modifier = Modifier
                .clickable(onClick = onItemClick)
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            BaseText(
                text = model.title
            )
        }
    }
}

