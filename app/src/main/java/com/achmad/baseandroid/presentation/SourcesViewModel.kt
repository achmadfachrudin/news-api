package com.achmad.baseandroid.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.achmad.baseandroid.core.network.ApiResult
import com.achmad.baseandroid.service.data.model.Category
import com.achmad.baseandroid.service.data.model.Source
import com.achmad.baseandroid.service.domain.GetNewsCategoriesUseCase
import com.achmad.baseandroid.service.domain.GetSourcesByCategoriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SourcesViewModel @Inject constructor(
    private val getSourcesByCategoriesUseCase: GetSourcesByCategoriesUseCase
) : ViewModel() {

    val uiMutableState = MutableStateFlow<ApiResult<List<Source>>>(ApiResult.Loading)
    val uiState: StateFlow<ApiResult<List<Source>>> = uiMutableState

    val queryMutableState = MutableStateFlow<String>("")
    val queryState: StateFlow<String> = queryMutableState

    fun getSources(categoryQuery: String) {
        viewModelScope.launch {
            getSourcesByCategoriesUseCase(categoryQuery).collectLatest {
                uiMutableState.value = it
            }
        }
    }

    fun onQueryChanged(newQuery: String) {
        viewModelScope.launch {
            queryMutableState.value = newQuery.trim().lowercase()
        }
    }
}
